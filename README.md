# install dependencies
```
(testCI) $ npm install
```
# start 
```
(testCI) $ npm start
```
# run javascript unit tests 
```
(testCI) $ npm test
```

# run typescript unit tests 
```
(testCI) $ npm run testTS
```

