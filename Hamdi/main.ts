export class Calculator {
    constructor() {
    }
    add(numberString: string) {
        let array = [];
        if(numberString.substring(0,2)=="//"){
            let lines = numberString.split("\n");
            array = lines[1].split(lines[0].substring(2));
        }else 
            array = numberString.split(/,|\n/);
        let sum = 0;
        for(var i = 0; i < array.length; i++){
            sum += Number(array[i]);
        }
        return sum;
    }
}