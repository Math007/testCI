class Operations {

    do(value1, value2) {
        throw new TypeError("Abstract class");
    }

    getStrValue() {
        throw new TypeError("Abstract class");
    }

}

class Addition extends Operations{

    do(var1, var2){
        return var1 + var2;
    }

    getStrValue(){
        return '+';
    }

}

class Subtraction extends Operations{

    do(var1, var2){
        return var1 - var2;
    }

    getStrValue(){
        return '-';
    }

}

class Division extends Operations{

    do(var1, var2){
        return var1 / var2;
    }

    getStrValue(){
        return '/';
    }

}

module.exports = {
    Addition : Addition,
    Subtraction : Subtraction,
    Division : Division
};

