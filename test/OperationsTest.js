const assert  = require("chai").assert;
const Addition = require("../Operations").Addition;
const Subtraction = require("../Operations").Subtraction;
const Division = require("../Operations").Division;

/**
 * This describe is for a single class (Addition in this case)
 */
describe('Additions class tests', function () {
    /**
     * This one contains the tests for first method do
     * it() contains each test case for a single method
     */
    describe("do(var1, var2)", function () {
        /**
         * Test case #1
         */
        it('returns the sum of two positive numbers', function () {
            const uut = new Addition();
            const result = uut.do(5, 6);
            assert.equal(result, 11);

        });

        /**
         * Test case #2
         */
        it('returns the sum of two negative numbers', function () {
            const uut = new Addition();
            const result = uut.do(-5, -6);
            assert.equal(result, -11);

        });
    });

    /**
     * Contains the test for the second method getStrValue
     */
    describe("getStrValue()", function () {
        /**
         * Test case #1
         */
        it('returns the string operator', function () {
            const uut = new Addition();
            assert.equal(uut.getStrValue(), '+');
        });
    });
});


/**
 * This describe is for a single class (Subtraction in this case)
 */
describe('Subtraction class tests', function () {

    describe("do(var1, var2)", function () {
        /**
         * Test case #1
         */
        it('returns the subtraction of two positive numbers', function () {
            const uut = new Subtraction();
            const result = uut.do(5, 6);
            assert.equal(result, -1);

        });

        /**
         * Test case #2
         */
        it('returns the subtraction of two negative numbers', function () {
            const uut = new Subtraction();
            const result = uut.do(-5, -6);
            assert.equal(result, 1);

        });
    });

    /**
     * Contains the test for the second method getStrValue
     */
    describe("getStrValue()", function () {
        /**
         * Test case #1
         */
        it('returns the string operator', function () {
            const uut = new Subtraction();
            assert.equal(uut.getStrValue(), '-');
        });
    });
});

/**
 * This describe is for a single class (Division in this case)
 */
describe('Division class tests', function () {

    describe("do(var1, var2)", function () {
        /**
         * Test case #1
         */
        it('returns the Division of two positive numbers', function () {
            const uut = new Division();
            const result = uut.do(10, 5);
            assert.equal(result, 2);

        });

        /**
         * Test case #2
         */
        it('returns the Division of two negative numbers', function () {
            const uut = new Division();
            const result = uut.do(-10, -5);
            assert.equal(result, 2);

        });
    });

    /**
     * Contains the test for the second method getStrValue
     */
    describe("getStrValue()", function () {
        /**
         * Test case #1
         */
        it('returns the string operator', function () {
            const uut = new Division();
            assert.equal(uut.getStrValue(), '/');
        });
    });
});