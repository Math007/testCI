const Addition = require("./Operations").Addition;

function main() {
    console.log("TEST");
    const addition = new Addition();
    const num1 = 9;
    const num2 = 4;

    const result = addition.do(num1, num2);

    console.log(result);

}

main();