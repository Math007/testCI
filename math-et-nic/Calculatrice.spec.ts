import {expect} from "chai";
import {Calculatrice} from "./Calculatrice";


/**
 * This describe is for a single class (Addition in this case)
 */
describe('Additions class tests', function () {
    let uut:Calculatrice;

    describe('function add', () => {

        it('should return 0 if the string is empty', () => {
            uut = new Calculatrice();
            const result = uut.add("");
            expect(result).to.equal(0);
        });

        it('should return the number if there is only one number', () => {
            uut = new Calculatrice();
           const result = uut.add("123");
           expect(result).to.equal(123);
        });

        it('should return the sum of two numbers', () => {
            uut = new Calculatrice();
            const result = uut.add("3,4");
            expect(result).to.equal(7)
        });

        it('should return the sum of three numbers', () => {
            uut = new Calculatrice();
            const result = uut.add("3,4,5");
            expect(result).to.equal(12);
        });

        it('should return the sum of three numbers with "\\n"', () => {
            uut = new Calculatrice();
            const result = uut.add("1\n2,3");
            expect(result).to.equal(6);
        });

    })

});
