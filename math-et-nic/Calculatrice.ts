export class Calculatrice {

    add(equation: String): number{
        const numberArray: String[]  = equation.split(/\n|,/);
        let result = 0;
        numberArray.forEach((number) => {
           result +=  Number(number);
        });

        return result;
    }
}
