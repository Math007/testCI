import {Calculator} from './main';
import {expect} from 'chai';
import 'mocha';

describe('testing Calculator class', function () {
    /**
     * This one contains the tests for first method do
     * it() contains each test case for a single method
     */
    describe("add(numberString: string)", function () {

        it('returns 0 for an empty string', function () {
            let calc = new Calculator();
            const result = calc.add("");
            expect(result).to.equal(0);

        });

        it('returns the value of the number if there is only one', function () {
            let calc = new Calculator();
            const result = calc.add("2");
            expect(result).to.equal(2);
        });

        it('returns the sum of two numbers', function () {
            let calc = new Calculator();
            const result = calc.add("2,3");
            expect(result).to.equal(5);
        });

        it('returns the sum of all numbers', function () {
            let calc = new Calculator();
            const result = calc.add("11,10,2,3,5,1,3");
            expect(result).to.equal(35);
        });

        it('returns the sum of all numbers even with \\n as separator', function () {
            let calc = new Calculator();
            const result = calc.add("1\n2,3");
            expect(result).to.equal(6);
        });

        it("returns the sum with custom separators", function () {
            let calc = new Calculator();
            const result = calc.add("//;\n1;2");
            expect(result).to.equal(3);
        });
    });

});